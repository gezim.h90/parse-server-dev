var express = require('express');
var ParseServer = require('/Users/gezimhaziri/workspace/javascript/parse-server/lib/index').ParseServer;
var app = express();

var api = new ParseServer({
  databaseURI: 'mongodb://localhost:27017/dev',
  appId: 'myAppId',
  masterKey: '123', //Add your master key here. Keep it secret!
  javascriptKey: '456',
  serverURL: 'http://localhost:1337/parse',  // Don't forget to change to https if needed
  liveQuery: {
    classNames: ["Posts", "Comments"] // List of classes to support for query subscriptions
  }
});
// Serve the Parse API on the /parse URL prefix
app.use('/parse', api);

app.listen(1337, function() {
  console.log('parse-server-example running on port 1337.');
});
